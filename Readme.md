# eks

This repos contains code for deploying a kubernetes cluster on AWS EKS using pulumi and gitlab.

## Documentation

- [Pulumi EKS](https://www.pulumi.com/docs/guides/crosswalk/aws/eks/)
- [Deplot to AWS from GitLab CI/CD](https://docs.gitlab.com/ee/ci/cloud_deployment/)
- [Pulumi Python](https://www.pulumi.com/docs/intro/languages/python/)
- [Pulumi GitLab CI](https://www.pulumi.com/docs/guides/continuous-delivery/gitlab-ci/)

## How the code in this repos was made

- Create a folder pulumi
- cd into the pulumi folder
- Run ```pulumi new```
- gitlab-ci.yml was edited folloing the instruction of this page [Pulumi GitLab CI](https://www.pulumi.com/docs/guides/continuous-delivery/gitlab-ci/)
- Then through this [merge request](https://gitlab.com/eks-explorer/eks/-/merge_requests/1) every error was fixed

## Permission of the AWS user

The AWS user needs to have the following permissions: https://eksctl.io/usage/minimum-iam-policies/

Also a policy need to be created

Name: PulumiEKSCluster-Strategy
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "iam:CreateRole",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:GetRole",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:ListRolePolicies",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:AttachRolePolicy",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:CreateInstanceProfile",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "eks:TagResource",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "eks:CreateCluster",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": "arn:aws:iam::*:role/my-cluster*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:RemoveRoleFromInstanceProfile",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:DeleteInstanceProfile",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:DetachRolePolicy",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:ListInstanceProfilesForRole",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:DeleteRole",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "ssm:GetParameter",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudformation:CreateStack",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudformation:DescribeStacks",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudformation:UpdateStack",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudformation:GetTemplate",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudformation:DeleteStack",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "eks:ListClusters",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:GetInstanceProfile",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:AddRoleToInstanceProfile",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:UpdateAssumeRolePolicy",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:ListAttachedRolePolicies",
            "Resource": "*"
        }
    ]
}
```

Then add it to the group or the user.

source: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_permissions-required.html

Also in the role section, the following instruction must be made: https://docs.aws.amazon.com/eks/latest/userguide/service_IAM_role.html#create-service-role

