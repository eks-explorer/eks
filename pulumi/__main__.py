# https://www.pulumi.com/docs/guides/crosswalk/aws/eks/
# https://www.pulumi.com/blog/aws-eks-managed-nodes-fargate/
import json
import pulumi
from pulumi_kubernetes import Provider
from pulumi_kubernetes.core.v1 import Namespace
import pulumi_eks as eks
import pulumi_aws as aws
from pulumi_kubernetes_ingress_nginx import IngressController, ControllerArgs, ControllerPublishServiceArgs
from deployItems.keycloak import DeployKeycloak

# Create an EKS cluster with the default configuration.
cluster = eks.Cluster('my-cluster-eks-explorer')

# https://github.com/pulumi/pulumi-eks/issues/662
provider = Provider("eks-k8s", kubeconfig=cluster.kubeconfig.apply(lambda k: json.dumps(k)))

# Deploy Ingress Controller
# https://www.pulumi.com/registry/packages/kubernetes-ingress-nginx/
ingress_controller = IngressController("myctrl", 
    controller=ControllerArgs(
        publish_service=ControllerPublishServiceArgs(
            enabled=True
        )
    ),
    opts=pulumi.ResourceOptions(provider=provider)
)

# export the controller hostname
pulumi.export('controller_hostname', ingress_controller.status.load_balancer.ingress[0].hostname)

# Create a namespace
namespaceName = "test-namespace"
namespace = Namespace(
    namespaceName,
    metadata={
        "name": namespaceName
    },
    opts=pulumi.ResourceOptions(provider=provider)
)

deployItems = [
    DeployKeycloak()
]

for deployItem in deployItems:
    resource = deployItem.deploy(provider, namespaceName)