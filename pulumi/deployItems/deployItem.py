import abc
import pulumi

class DeployItem(abc.ABC):

    def __init__(self):
        pass

    @abc.abstractmethod
    def deploy(self, provider: pulumi.ProviderResource, namespaceName: str) -> pulumi.ComponentResource:
        pass