from deployItems.deployItem import DeployItem
from pulumi import ResourceOptions
import pulumi_kubernetes as k8s

class DeployKeycloak(DeployItem):
    def deploy(self, provider, namespaceName):
        postgresql_deployment = k8s.apps.v1.Deployment("postgresql-deployment",
            metadata={
                "namespace": namespaceName,
                "name": "postgresql-deployment",
            },
            spec={
                "replicas": 1,
                "selector": {
                    "match_labels": {
                        "app.kubernetes.io/name": "postgresql-app",
                    },
                },
                "template": {
                    "metadata": {
                        "labels": {
                            "app.kubernetes.io/name": "postgresql-app",
                        },
                    },
                    "spec": {
                        "containers": [{
                            "name": "postgresql-keycloak",
                            "image": "postgres",
                            "ports": [{
                                "containerPort": 5432,
                            }],
                            "env": [{
                                "name": "POSTGRES_USER",
                                "value": "keycloak",
                            }, {
                                "name": "POSTGRES_PASSWORD",
                                "value": "keycloak",
                            }, {
                                "name": "POSTGRES_DB",
                                "value": "keycloak",
                            }]
                        }],
                    },
                },
            },
            opts=ResourceOptions(provider=provider))

        # Create a NodePort service for postgresql
        postgresql_service = k8s.core.v1.Service("postgresql-service",
            metadata={
                "namespace": namespaceName,
                "name": "postgresql-service",
            },
            spec={
                "type": "NodePort",
                "ports": [{
                    "port": 5432,
                    "targetPort": 5432,
                }],
                "selector": {
                    "app.kubernetes.io/name": "postgresql-app",
                },
            },
            opts=ResourceOptions(provider=provider))

        # Create a keycloak release
        keycloak_release = k8s.helm.v3.Release("keycloak",
            chart="keycloak",
            namespace=namespaceName,
            repository_opts=k8s.helm.v3.RepositoryOptsArgs(
                repo="https://charts.bitnami.com/bitnami",
            ),
            values={
                "fullnameOverride": "keycloak",
                "ingress": {
                    "enabled": True,
                },
                "auth": {
                    "adminUser": "admin",
                    "adminPassword": "admin",
                },
                "service": {
                    "type": "ClusterIP",
                },
                "postgresql": {
                    "enabled": False,
                },
                "externalDatabase": {
                    "host": postgresql_service.metadata["name"],
                    "database": "keycloak",
                    "user": "keycloak",
                    "password": "keycloak",
                },
            },
            opts=ResourceOptions(provider=provider, depends_on=[postgresql_deployment])
        )
